export const QuestionAPI = {
    fetchQuestions(amount, category, difficulty, type) {
        
        return fetch(`https://opentdb.com/api.php?amount=${amount}&category=${category}&difficulty=${difficulty}&type=${type}`)
            .then(response => response.json())


    },
    fetchQuestionCategories() {
        return fetch("https://opentdb.com/api_category.php")
            .then(response => response.json())

    }
}
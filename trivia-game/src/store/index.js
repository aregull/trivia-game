import Vue from 'vue';
import Vuex from 'vuex';
import { QuestionAPI } from '../components/Questions/QuestionAPI';

Vue.use(Vuex);

// The place to store all your state in one place.
export default new Vuex.Store({
    state: {
        questions: [],
        question: '',
        difficulty: '', //easy, medium, hard
        category: '', //id
        amount: '', //max 20
        type: '', //multiple, boolean
        questionNumber: '',
        answers: [],
        userAnswers: [],
        score: 0,
        round: 0,
        finished: '',
        error: '',
        correctAnswers: [],
        questionArray: [],
    },
    mutations: {
        setQuestions: (state, payload) => {
            state.questions = payload;
        },
        setQuestion: (state, payload) => {
            state.question = payload;
        },
        setDifficulty: (state, payload) => {
            state.difficulty = payload;
        },
        setCategory: (state, payload) => {
            state.category = payload;
        },
        setCategories: (state, payload) => {
            state.categories = payload;
        },
        setAmount: (state, payload) => {
            state.amount = payload;
        },
        setType: (state, payload) => {
            state.type = payload;
        },
        setQuestionNumber: (state, payload) => {
            state.questionNumber = payload;
        },
        setAnswers: (state, payload) => {
            state.answers = payload;
        },
        setUserAnswers: (state, payload) => {
            state.userAnswers = payload;
        },
        setScore: (state, payload) => {
            state.score = payload;
        },
        setRound: (state, payload) => {
            state.round = payload
        },
        setFinished: (state, payload) => {
            state.finished = payload
        },
        setError: (state, payload) => {
            state.error = payload;
        },
        setCorrectAnswers: (state, payload) => {
            state.correctAnswers = payload;
        },
        setQuestionArray: (state, payload) => {
            state.questionArray = payload;
        },
    },
    getters: {

    },
    actions: {
        async fetchQuestions({ commit }) {
            try {
                const allQuestions = await QuestionAPI.fetchQuestions(
                    this.state.amount,
                    this.state.category,
                    this.state.difficulty,
                    this.state.type
                )
                commit('setRound', 0)

                if (this.state.type == 'multiple') {
                    let firstAnswerOptions = allQuestions.results[this.state.round].incorrect_answers.concat(allQuestions.results[this.state.round].correct_answer)
                    let currentIndex = firstAnswerOptions.length, randomIndex;

                    // While there remain elements to shuffle...
                    while (currentIndex != 0) {

                        // Pick a remaining element...
                        randomIndex = Math.floor(Math.random() * currentIndex);
                        currentIndex--;

                        // And swap it with the current element.
                        [firstAnswerOptions[currentIndex], firstAnswerOptions[randomIndex]] = [
                            firstAnswerOptions[randomIndex], firstAnswerOptions[currentIndex]];
                    }

                    commit('setAnswers', firstAnswerOptions);
                }

                for (let i = 0; i < this.state.amount; i++) {
                    commit('setCorrectAnswers', this.state.correctAnswers.concat(allQuestions.results[i].correct_answer));
                    commit('setQuestionArray', this.state.questionArray.concat(allQuestions.results[i].question));
                }


                commit('setQuestions', allQuestions);
                commit('setQuestion', allQuestions.results[this.state.round].question);


            } catch (e) {
                console.log('ERROR IN FETCHQUESTIONS ' + e.message)
                this.error = e.message;
            }
        },
        //under actions:
        saveAnswers({ commit }, incomingValue) {

            if (incomingValue == this.state.questions.results[this.state.round].correct_answer) {
                commit('setScore', this.state.score + 10)
            }

            commit('setUserAnswers', this.state.userAnswers.concat(incomingValue))
        },
        nextRound({ commit }) {

            if (this.state.round + 1 == this.state.amount) {
                console.log(this.state.round + this.state.amount)
                commit('setFinished', 'finished')
            } else {
                commit('setRound', this.state.round + 1)
                if (this.state.type == 'multiple') {
                    let answerOptions = this.state.questions.results[this.state.round].incorrect_answers.concat(this.state.questions.results[this.state.round].correct_answer)
                    let currentIndex = answerOptions.length, randomIndex;

                    // While there remain elements to shuffle...
                    while (currentIndex != 0) {

                        // Pick a remaining element...
                        randomIndex = Math.floor(Math.random() * currentIndex);
                        currentIndex--;

                        // And swap it with the current element.
                        [answerOptions[currentIndex], answerOptions[randomIndex]] = [
                            answerOptions[randomIndex], answerOptions[currentIndex]];
                    }
                    commit('setAnswers', answerOptions);
                }
            }
        },
        async playAgain({ commit }) {
            commit('setScore', 0);
            commit('setQuestionArray', [])
            commit('setUserAnswers', [])
            commit('setQuestions', [])
            commit('setQuestion', '')
            commit('setCorrectAnswers', [])
            commit('setFinished', '')

            try {
                const allQuestions = await QuestionAPI.fetchQuestions(
                    this.state.amount,
                    this.state.category,
                    this.state.difficulty,
                    this.state.type
                )
                commit('setRound', 0)

                if (this.state.type == 'multiple') {
                    let firstAnswerOptions = allQuestions.results[this.state.round].incorrect_answers.concat(allQuestions.results[this.state.round].correct_answer)
                    let currentIndex = firstAnswerOptions.length, randomIndex;

                    // While there remain elements to shuffle...
                    while (currentIndex != 0) {

                        // Pick a remaining element...
                        randomIndex = Math.floor(Math.random() * currentIndex);
                        currentIndex--;

                        // And swap it with the current element.
                        [firstAnswerOptions[currentIndex], firstAnswerOptions[randomIndex]] = [
                            firstAnswerOptions[randomIndex], firstAnswerOptions[currentIndex]];
                    }

                    commit('setAnswers', firstAnswerOptions);
                }

                for (let i = 0; i < this.state.amount; i++) {
                    commit('setCorrectAnswers', this.state.correctAnswers.concat(allQuestions.results[i].correct_answer));
                    commit('setQuestionArray', this.state.questionArray.concat(allQuestions.results[i].question));
                }


                commit('setQuestions', allQuestions);
                commit('setQuestion', allQuestions.results[this.state.round].question);


            } catch (e) {
                console.log('ERROR IN FETCHQUESTIONS ' + e.message)
                this.error = e.message;
            }
        },
        resetStates({ commit }) {
            commit('setScore', 0);
            commit('setQuestionArray', []);
            commit('setUserAnswers', [])
            commit('setQuestions', [])
            commit('setCorrectAnswers', [])
            commit('setFinished', '')
            commit('setDifficulty', '')
            commit('setType', '')
            commit('setCategory', '')
            commit('setAmount', 10)
        },

        async fetchQuestionCategories({ commit }) {
            try {
                const categories = await QuestionAPI.fetchQuestionCategories();
                commit('setCategories', categories);

            } catch (e) {
                commit('setError', e.message);
            }
        },
    },
})
import Vue from 'vue';
import VueRouter from 'vue-router';

/**
 * Router for the trivia app. Contains three pages, the start screen, game screen and results screen.
 */

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Start',
        component: () => import(/* webpackChunkName: "start"*/ '../components/Startscreen/Startscreen.vue')
    },
    {
        path: '/trivia',
        name: 'Trivia',
        component: () => import(/* webpackChunkName: "trivia"*/ '../components/Triviascreen/Triviascreen.vue')
    },
    {
        path: '/results',
        name: 'Results',
        component: () => import(/* webpackChunkName: "results"*/ '../components/Results/Results.vue')
    }
    ]

const router = new VueRouter({ 
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router;
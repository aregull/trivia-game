# trivia-game

Trivia Game made with Vue.js, questions are fetched from https://opentdb.com/.

## Known bugs and what to fix next
- Will add comments to the code to make it clearer
- Could have done toLowerCase when checking if the chosen answer is equal to the correct answer to filter out any mistakes.
- Have not been able to check if the check for correct answer handles the fact that some strings come with symbols, but the values from the answer buttons are only letters.
- Background is not sticky, if you have chosen 10+ questions you might see the background image repeat itself on the result screen.
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
